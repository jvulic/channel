# Channel

[GoDoc]: https://godoc.org/bitbucket.org/jvulic/channel
[GoDoc Widget]: https://godoc.org/bitbucket.org/jvulic/channel?status.png
[Go Report Card]: https://goreportcard.com/report/bitbucket.org/jvulic/channel
[Go Report Card Widget]: https://goreportcard.com/badge/bitbucket.org/jvulic/channel

[![Go Doc][GoDoc Widget]][GoDoc] [![Go Report Card][Go Report Card Widget]][Go Report Card]

## Introduction

Channel is an extension to Go channels providing flexible capacity along with
pluggable at-capacity behaviour and size alerts.

## Install

go get -v bitbucket.org/jvulic/channel

## Usage

Creating a channel.

```go
channel.New() // default infinite capacity channel
channel.New(channel.Capacity(10, channel.RemoveOldest)) // channel with remove-oldest at capacity behaviour
channel.New(channel.SizeAlert(5, func(size int) { fmt.Printf("oh no! %v", size) })) // Channel with size alert
```

General use -- follows from how one might expect a channel to behave.

```go
// Create a channel.
ch := channel.New()

// Add 100 elements to the channel in a separate go channel.
go func() {
  for i := 0; i < 100; i++ {
    ch.In() <- i
  }
  ch.Close()
}

// Read out those 100 elements.
for {
  elem, ok := <-ch.Out()
  if !ok {
    break // Output buffer has been closed
  }
  fmt.Printf(elem)
}
```

## Tests

Tests can be run by executing `make test`.

## License
[MIT License](LICENSE)
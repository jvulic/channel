package channel

import (
	"bitbucket.org/jvulic/queue"
)

/*
	Defines the structure for optional behaviour plugins and provides some
	common behaviour tropes.
*/

// CapacityBehaviourFunc defines the function called when the channel is at
// capacity.
// elem - the next element to be processed;
// buf - internal queue buffer.
type CapacityBehaviourFunc func(elem interface{}, buf queue.Queue)

// RemoveOldest defines a CapacityBehaviourFunc that removes the oldest entry
// and adds the new element to the internal buffer.
func RemoveOldest(elem interface{}, buf queue.Queue) {
	buf.Pop()
	buf.Push(elem)
}

// RemoveNewest defines a CapacityBehaviourFunc that throws away the new
// element.
func RemoveNewest(elem interface{}, buf queue.Queue) {}

// LetItGo defines a CapacityBehaviourFunc that ignores the capacity and
// simply continues to add the internal buffer.
func LetItGo(elem interface{}, buf queue.Queue) {
	buf.Push(elem)
}

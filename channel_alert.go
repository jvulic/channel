package channel

// SizeAlertFunc defines the method is called when the internal buffer reaches
// an assigned threshold.
// size - the current size of the channel.
type SizeAlertFunc func(size int)

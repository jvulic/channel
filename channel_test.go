package channel

import (
	"testing"
	"time"
)

func timeout(duration time.Duration) chan struct{} {
	timeout := make(chan struct{})
	go func() {
		time.Sleep(duration)
		close(timeout)
	}()
	return timeout
}

func TestBasic(t *testing.T) {
	nElements := 997

	ch := New()
	go func() {
		for i := 0; i < nElements; i++ {
			ch.In() <- i
		}
		ch.Close()
	}()

	time := timeout(time.Second * 5)
	i := 0
	for {
		select {
		case elem, ok := <-ch.Out():
			if !ok {
				return // Finished
			}
			if elem.(int) != i {
				t.Fatalf("Expected %v received %v", i, elem)
			}
			i++
		case <-time:
			t.Fatal("TestBasic timed out")
		}
	}
}

func TestRemoveOldest(t *testing.T) {
	ch := New(
		Capacity(2, RemoveOldest),
	)

	// Adding 3 entries into the channel should overwrite the first.
	// <- 1, 2
	for i := 0; i < 3; i++ {
		ch.In() <- i
	}

	for i := 1; i < 3; i++ {
		if out := <-ch.Out(); out != i {
			t.Fatalf("Expected %v received %v from channel", i, out)
		}
	}
}

func TestRemoveNewest(t *testing.T) {
	ch := New(
		Capacity(2, RemoveNewest),
	)

	// Adding 3 entries into the channel should never include the 3rd entry.
	// <- 0, 1
	for i := 0; i < 3; i++ {
		t.Logf("Adding %v", i)
		ch.In() <- i
	}

	for i := 0; i < 2; i++ {
		if out := <-ch.Out(); out != i {
			t.Fatalf("Expected %v received %v from channel", i, out)
		}
	}
}

func TestLetItGo(t *testing.T) {
	ch := New(
		Capacity(2, LetItGo),
	)

	// Adding 3 entries into the channel should do nothing. The channel should
	// just continue adding entires.
	// <- 0, 1, 2
	for i := 0; i < 3; i++ {
		t.Logf("Adding %v", i)
		ch.In() <- i
	}

	for i := 0; i < 3; i++ {
		if out := <-ch.Out(); out != i {
			t.Fatalf("Expected %v received %v from channel", i, out)
		}
	}
}

func TestSizeAlert(t *testing.T) {
	time := timeout(time.Second * 5)

	done := make(chan struct{})
	funky := func(int) {
		close(done)
	}
	ch := New(
		SizeAlert(0, funky),
	)

	ch.In() <- 1

	select {
	case <-done:
	case <-time:
		t.Fatal("TestSizeAlert timed out waiting for alert")
	}
}

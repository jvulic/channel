/*
Package channel is a go-like channel with flexible capacity contraints.
*/
package channel

import (
	"bitbucket.org/jvulic/queue"
)

/*
	Channel is a go-like channel with flexible capacity contraints.

	It achieves this flexibility by heavy utilization of internal queue as the
	storage mechanism.
*/

type options struct {
	capacityActive   bool
	capacity         int
	capBehaviourFunc CapacityBehaviourFunc

	sizeAlertActive    bool
	sizeAlertThreshold int
	sizeAlertFunc      SizeAlertFunc
}

// Option configures aspects of the channel.
type Option func(*options)

// Capacity returns an Option that sets the capacity of the channel and its
// at-capacity behaviour.
func Capacity(capacity int, atCapFunc CapacityBehaviourFunc) Option {
	switch {
	case capacity < 1:
		panic("Attempting to set capacity less than 1")
	case atCapFunc == nil:
		panic("Attempting to set CapacityBehaviourFunc to nil")
	}
	return func(o *options) {
		o.capacityActive = true
		o.capacity = capacity
		o.capBehaviourFunc = atCapFunc
	}
}

// SizeAlert returns a Option that sets the capacity alert threshold and alert
// function.
func SizeAlert(threshold int, alertFunc SizeAlertFunc) Option {
	if alertFunc == nil {
		panic("Attempting to set SizeAlertFunc to nil")
	}
	return func(o *options) {
		o.sizeAlertActive = true
		o.sizeAlertThreshold = threshold
		o.sizeAlertFunc = alertFunc
	}
}

// Channel is a augmented go-like channel.
type Channel struct {
	opts options

	input, output, quit chan interface{}
	buf                 queue.Queue
}

// New creates a new channel. The default channel is an infinite capacity channel
// with no at-capacity behaviour and no size alerts.
func New(opt ...Option) *Channel {
	var opts options
	for _, o := range opt {
		o(&opts)
	}

	ch := &Channel{
		opts:   opts,
		input:  make(chan interface{}),
		output: make(chan interface{}),
		quit:   make(chan interface{}),
		buf:    queue.New(),
	}
	go ch.contoller()
	return ch
}

func (ch *Channel) contoller() {
	defer close(ch.output)

	var closed bool
	for {
		switch ch.buf.Length() {
		case 0: // Buffer is empty.
			select {
			case elem := <-ch.input:
				ch.add(elem)
			case <-ch.quit:
				return
			}
		default:
			next := ch.buf.Front()

			select {
			case elem := <-ch.input:
				if !closed {
					ch.add(elem)
				}
			case ch.output <- next:
				ch.buf.Pop()
			case <-ch.quit:
				// Standard quit exits only after the output buffer has been emptied.
				// We continue the control loop until we empty the output buffer,
				// throwing away any new input elements.
				closed = true
			}
		}
	}
}

func (ch *Channel) add(elem interface{}) {
	if ch.opts.sizeAlertActive && ch.buf.Length() >= ch.opts.sizeAlertThreshold {
		// We have hit the size alert threshold.
		ch.opts.sizeAlertFunc(ch.buf.Length())
	}

	if ch.opts.capacityActive && ch.buf.Length() >= ch.opts.capacity {
		// We are at capacity.
		ch.opts.capBehaviourFunc(elem, ch.buf)
	} else {
		ch.buf.Push(elem)
	}

}

// Close stops the internal contoller mechanism within channel. needs to be
// called at least once to allow the goroutine to exit.
func (ch *Channel) Close() {
	close(ch.quit)
}

// In provides incoming access to the internal input channel.
func (ch *Channel) In() chan<- interface{} {
	return ch.input
}

// Out provides outgoing access to the internal output channel.
func (ch *Channel) Out() <-chan interface{} {
	return ch.output
}
